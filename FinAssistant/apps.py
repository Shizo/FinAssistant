from django.apps import AppConfig


class FinassistantConfig(AppConfig):
    name = 'FinAssistant'
